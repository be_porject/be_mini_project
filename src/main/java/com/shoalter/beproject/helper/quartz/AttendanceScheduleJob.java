package com.shoalter.beproject.helper.quartz;

import com.shoalter.beproject.controller.attendance.service.AttendanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
@RequiredArgsConstructor
public class AttendanceScheduleJob implements Job {

    private final AttendanceService attendanceService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            log.info("=== Start schedule task for count working hours, current time：" + LocalDateTime.now());
            attendanceService.getAttendanceRecord();
        } catch (Exception e) {
            log.error("Schedule Job ERROR: " + e.getMessage());
        }
    }

}
