package com.shoalter.beproject.dao;

import lombok.Value;

import java.io.Serializable;

@Value
public class ResponseDto<T> implements Serializable {
    private static final long serialVersionUID = -8071935146593886203L;
    Status status;

    T data;

    public static <T> ResponseDto<T> success(T data) {
        return new ResponseDto<>("success", data);
    }

    public static <T> ResponseDto<T> fail(String code, String message) {
        return new ResponseDto<>(code, message);
    }

    public ResponseDto(String code, String message) {
        this.status = new Status(code, message);
        this.data = null;
    }

    private ResponseDto(String code, T data) {
        this.status = new Status(code, null);
        this.data = data;
    }

    @Value
    public static class Status implements Serializable {

        private static final long serialVersionUID = 3780903680181947353L;

        String code;

        String message;
    }
}
