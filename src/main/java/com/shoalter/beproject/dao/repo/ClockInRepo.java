package com.shoalter.beproject.dao.repo;

import com.shoalter.beproject.dao.entity.ClockInRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClockInRepo extends JpaRepository<ClockInRecordEntity, Integer> {
    @Query(value = "SELECT id, username, MIN(clock_in_time) as clock_in_time " +
            "FROM clock_in_record " +
            "WHERE DATE(clock_in_time) LIKE :date " +
            "GROUP BY username ", nativeQuery = true)
    List<ClockInRecordEntity> findOldestClockInTime(String date);

}
