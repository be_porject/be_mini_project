package com.shoalter.beproject.dao.repo;

import com.shoalter.beproject.dao.entity.AttendanceRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceRepo extends JpaRepository<AttendanceRecordEntity, Integer> {
}
