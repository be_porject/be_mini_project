package com.shoalter.beproject.dao.repo;

import com.shoalter.beproject.dao.entity.ClockOutRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClockOutRepo extends JpaRepository<ClockOutRecordEntity, Integer> {

    @Query(value = "SELECT id, username, MAX(clock_out_time) as clock_out_time " +
            "FROM clock_out_record " +
            "WHERE DATE(clock_out_time) LIKE :date " +
            "GROUP BY username ", nativeQuery = true)
    List<ClockOutRecordEntity> findNewestClockOutTime(String date);
}
