package com.shoalter.beproject.dao.repo;

import com.shoalter.beproject.dao.entity.AccountEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Optional;

@Mapper
public interface AccountMapper {
    @Select("select * from account where username = #{username} ")
    Optional<AccountEntity> findByUsername(String username);
}
