package com.shoalter.beproject.exception;

public class AccountException extends RuntimeException {
    private static final long serialVersionUID = 8462625351005212972L;

    public AccountException(String message) {
        super(message);
    }

    public AccountException(String message, Exception e) {
        super(message, e);
    }
}
