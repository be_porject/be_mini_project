package com.shoalter.beproject.exception;

import com.shoalter.beproject.enums.ErrorCode;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.security.core.AuthenticationException;

@EqualsAndHashCode(callSuper = true)
@Value
public class InvalidTokenException extends AuthenticationException {

    private static final long serialVersionUID = -5900313845140719026L;

    ErrorCode errorCode;

    String message;

    public InvalidTokenException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }
}

