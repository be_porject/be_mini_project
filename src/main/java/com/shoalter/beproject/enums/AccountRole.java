package com.shoalter.beproject.enums;

public enum AccountRole {
    ADMIN,
    USER
}
