package com.shoalter.beproject.mapper.accountinfo;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import org.springframework.stereotype.Component;

@Component
public class AccountMapperToResponse {

    public AccountDtoResponse mapToResponse(AccountEntity entity) {
        var response = new AccountDtoResponse();
        if (entity == null) {
            return response;
        }
        response.setId(entity.getId());
        response.setUsername(entity.getUsername());
        response.setName(entity.getName());
        response.setRole(entity.getRole());
        return response;
    }
}
