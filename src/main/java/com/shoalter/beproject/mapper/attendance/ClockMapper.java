package com.shoalter.beproject.mapper.attendance;

import com.shoalter.beproject.controller.attendance.pojo.ClockResponse;
import com.shoalter.beproject.dao.entity.ClockInRecordEntity;
import com.shoalter.beproject.dao.entity.ClockOutRecordEntity;
import org.springframework.stereotype.Component;

@Component
public class ClockMapper {

    public ClockResponse clockInMapToResponse(ClockInRecordEntity entity, String type) {
        var response = new ClockResponse();
        if (entity == null) {
            return response;
        }
        response.setType(type);
        response.setUsername(entity.getUsername());
        response.setRecordTime(entity.getClockInTime());
        return response;
    }

    public ClockResponse clockOutMapToResponse(ClockOutRecordEntity entity, String type) {
        var response = new ClockResponse();
        if (entity == null) {
            return response;
        }
        response.setType(type);
        response.setUsername(entity.getUsername());
        response.setRecordTime(entity.getClockOutTime());
        return response;
    }

}
