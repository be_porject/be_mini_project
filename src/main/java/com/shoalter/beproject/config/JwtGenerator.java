package com.shoalter.beproject.config;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.dto.TokenDto;
import com.shoalter.beproject.exception.AccountException;
import com.shoalter.beproject.exception.InvalidTokenException;
import io.jsonwebtoken.*;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;

import static com.shoalter.beproject.enums.ErrorCode.*;

@Component
public class JwtGenerator {

    @Value(value = "${jwt.secret}")
    private String secret;

    @Value(value = "${jwt.expiration-second}")
    private Integer expirationSecond;

    public String generateToken(AccountDtoResponse accountDtoResponse) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", accountDtoResponse.getId());
        claims.put("username", accountDtoResponse.getUsername());
        claims.put("name", accountDtoResponse.getName());
        claims.put("role", accountDtoResponse.getRole());

        Date expireDate = new Date(System.currentTimeMillis() + expirationSecond * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(accountDtoResponse.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public Authentication validateToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token);
            TokenDto tokenAccount = toAccountInfo(token);
            Collection<? extends GrantedAuthority> authorities = getAuthorities(tokenAccount);
            return new TokenAuthenticatedUser(tokenAccount, token, authorities);
        } catch (MalformedJwtException e) {
            throw new InvalidTokenException(INVALID_JWT_TOKEN, "Invalid JWT token");
        } catch (ExpiredJwtException e) {
            throw new InvalidTokenException(EXPIRED_JWT_TOKEN, "Expired JWT token");
        } catch (UnsupportedJwtException e) {
            throw new InvalidTokenException(UNSUPPORTED_JWT_TOKEN, "Unsupported JWT token");
        } catch (IllegalArgumentException e) {
            throw new InvalidTokenException(INVALID_JWT_COMPACT_HANDLER, "JWT token compact of handler is invalid");
        } catch (Exception e) {
            throw new InvalidTokenException(UNEXPECTED_JWT_TOKEN_ERROR, e.getMessage());
        }
    }

    public TokenDto toAccountInfo(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        TokenDto tokenDto = new TokenDto();
        tokenDto.setId((Integer) claims.get("id"));
        tokenDto.setName(claims.get("name").toString());
        tokenDto.setRole(claims.get("role").toString());
        tokenDto.setUsername(claims.get("username").toString());
        return tokenDto;
    }

    public List<SimpleGrantedAuthority> getAuthorities(TokenDto tokenDto) {
        // TODO: get authorities of the given user when needed in the future
        return new ArrayList<>();
    }

    private Claims getAllClaims(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }
        return claims;
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = getAllClaims(token).get("sub").toString();
        return (username.equals(userDetails.getUsername()));
    }

    public String isSubValid(String token) {
        if (getAllClaims(token).get("sub") == null) {
            throw new AccountException("JWT token don't have subject");
        } else {
            return getAllClaims(token).get("sub").toString();
        }
    }

    @EqualsAndHashCode(callSuper = true)
    private static class TokenAuthenticatedUser extends AbstractAuthenticationToken {

        private static final long serialVersionUID = 5961458031432777403L;

        private final TokenDto principal;

        private String credentials;

        private TokenAuthenticatedUser(TokenDto principal, String credentials, Collection<? extends GrantedAuthority> authorities) {
            super(authorities);
            this.principal = principal;
            this.credentials = credentials;
            super.setAuthenticated(true); // must use super, as we override
        }

        @Override
        public String getCredentials() {
            return this.credentials;
        }

        @Override
        public TokenDto getPrincipal() {
            return this.principal;
        }

        @Override
        public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
            Assert.isTrue(!isAuthenticated,
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
            super.setAuthenticated(false);
        }

        @Override
        public void eraseCredentials() {
            super.eraseCredentials();
            this.credentials = null;
        }
    }

}