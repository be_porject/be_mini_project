package com.shoalter.beproject.config;

import com.shoalter.beproject.helper.quartz.AttendanceScheduleJob;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    @Value(value = "${spring.quartz.schedule}")
    private String scheduleTime;

    @Bean
    public JobDetail attendanceJobDetail() {
        return JobBuilder.newJob(AttendanceScheduleJob.class)
                .withIdentity("attendanceRecordJobDetail")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger attendanceTrigger(JobDetail attendanceRecordJobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(attendanceRecordJobDetail)
                .withIdentity("attendanceRecordTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(scheduleTime))
                .build();
    }

}
