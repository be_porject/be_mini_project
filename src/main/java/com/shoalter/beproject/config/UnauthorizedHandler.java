package com.shoalter.beproject.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shoalter.beproject.dao.ResponseDto;
import com.shoalter.beproject.exception.InvalidTokenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class UnauthorizedHandler implements AuthenticationEntryPoint {

    private static final ObjectMapper jsonObjectMapper = new ObjectMapper();

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException {
        log.error("Unauthorized error: {}", authException.getMessage());

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        String code = "-1";

        if (authException instanceof InvalidTokenException) {
            code = ((InvalidTokenException) authException).getErrorCode().getCode();
        }

        jsonObjectMapper.writeValue(response.getOutputStream(), ResponseDto.fail(code, authException.getMessage()));

    }
}