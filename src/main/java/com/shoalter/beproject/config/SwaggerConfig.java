package com.shoalter.beproject.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.http.HttpHeaders;

@OpenAPIDefinition(info = @Info(title = "Backend Project"),
        security = @SecurityRequirement(name = HttpHeaders.AUTHORIZATION))
@SecurityRequirement(name = "Bearer Authentication")
@SecurityScheme(
        name = HttpHeaders.AUTHORIZATION,
        type = SecuritySchemeType.HTTP,
        scheme = "Bearer"
)
public class SwaggerConfig {
}
