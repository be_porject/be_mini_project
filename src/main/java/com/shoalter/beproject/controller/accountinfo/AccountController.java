package com.shoalter.beproject.controller.accountinfo;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.controller.accountinfo.pojo.ResetPasswordRequest;
import com.shoalter.beproject.controller.accountinfo.service.GetAccountService;
import com.shoalter.beproject.controller.accountinfo.service.GetAllAccountService;
import com.shoalter.beproject.controller.accountinfo.service.ResetPasswordService;
import com.shoalter.beproject.dao.ResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/account")
@Tag(name = "Account 資訊")
public class AccountController {

    private final GetAccountService getAccountService;

    private final GetAllAccountService getAllAccountService;

    private final ResetPasswordService resetPasswordService;

    @Operation(summary = "獲取帳號", description = "獲取帳號資訊")
    @GetMapping(value = "/{username}")
    public ResponseDto<AccountDtoResponse> getAccount(@PathVariable String username) {
        return ResponseDto.success(getAccountService.findByUsername(username));
    }

    @Operation(summary = "獲取所有帳號", description = "獲取所有帳號資訊")
    @GetMapping
    public ResponseDto<List<AccountDtoResponse>> getAllAccount() {
        return ResponseDto.success(getAllAccountService.findAllUsername());
    }

    @Operation(summary = "更改密碼", description = "更改帳號密碼")
    @PostMapping("/resetPassword")
    public ResponseDto<String> resetPassword(@RequestBody ResetPasswordRequest resetPasswordRequest) {
        return ResponseDto.success(resetPasswordService.resetPassword(resetPasswordRequest));
    }

}
