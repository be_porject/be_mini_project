package com.shoalter.beproject.controller.accountinfo.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AccountDtoRequest {

    @Schema(title = "使用者帳號", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(title = "使用者密碼", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;

    @Schema(title = "使用者名字")
    private String name;
}
