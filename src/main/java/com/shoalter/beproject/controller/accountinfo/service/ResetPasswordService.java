package com.shoalter.beproject.controller.accountinfo.service;

import com.shoalter.beproject.controller.accountinfo.pojo.ResetPasswordRequest;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.repo.AccountRepo;
import com.shoalter.beproject.exception.AccountException;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ResetPasswordService {

    private final AccountRepo accountRepo;

    public String resetPassword(ResetPasswordRequest resetPasswordRequest) {
        String username = resetPasswordRequest.getUsername();
        String oldPassword = resetPasswordRequest.getOldPassword();
        String newPassword = resetPasswordRequest.getNewPassword();
        String confirmNewPassword = resetPasswordRequest.getConfirmNewPassword();
        AccountEntity findAccountInfo = accountRepo.findByUsername(username).orElseThrow(() -> {
            throw new AccountException("This username " + username + " doesn't exist.");
        });

        if (!Objects.equals(newPassword, confirmNewPassword)) {
            throw new AccountException("The new passwords do not match, please reconfirm it.");
        }
        if (!BCrypt.checkpw(oldPassword, findAccountInfo.getPassword())) {
            throw new AccountException("The old password is wrong.");
        }
        findAccountInfo.setPassword(BCrypt.hashpw(confirmNewPassword, BCrypt.gensalt()));
        accountRepo.save(findAccountInfo);

        return "Your Password is reset.";
    }

}
