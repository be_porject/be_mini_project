package com.shoalter.beproject.controller.accountinfo;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoRequest;
import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.controller.accountinfo.pojo.LoginRequest;
import com.shoalter.beproject.controller.accountinfo.pojo.LoginResponse;
import com.shoalter.beproject.controller.accountinfo.service.CreateAccountService;
import com.shoalter.beproject.controller.accountinfo.service.LoginService;
import com.shoalter.beproject.dao.ResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/auth")
@Tag(name = "認證資訊")
public class AuthController {

    private final CreateAccountService createAccountService;

    private final LoginService loginService;

    @Operation(summary = "註冊帳號", description = "創建新的帳號")
    @PostMapping
    public ResponseDto<AccountDtoResponse> createAccount(Locale locale, @RequestBody AccountDtoRequest accountDtoRequest) {
        return ResponseDto.success(createAccountService.createAccount(locale, accountDtoRequest));
    }

    @Operation(summary = "登入", description = "登入帳號")
    @PostMapping("/login")
    public ResponseDto<LoginResponse> loginAccount(@RequestBody LoginRequest loginRequest) {
        return ResponseDto.success(loginService.loginAccount(loginRequest.getUsername(), loginRequest.getPassword()));
    }

}
