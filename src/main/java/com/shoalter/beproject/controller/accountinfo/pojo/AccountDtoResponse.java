package com.shoalter.beproject.controller.accountinfo.pojo;

import com.shoalter.beproject.enums.AccountRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AccountDtoResponse {

    @Schema(title = "編號")
    private int id;

    @Schema(title = "使用者帳號")
    private String username;

    @Schema(title = "使用者名字")
    private String name;

    @Schema(title = "權限")
    private AccountRole role;
}
