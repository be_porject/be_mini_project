package com.shoalter.beproject.controller.accountinfo.service;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoRequest;
import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.repo.AccountRepo;
import com.shoalter.beproject.enums.AccountRole;
import com.shoalter.beproject.exception.AccountException;
import com.shoalter.beproject.mapper.accountinfo.AccountMapperToResponse;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class CreateAccountService {
    private final AccountRepo accountRepo;
    private final AccountMapperToResponse accountMapperToResponse;
    private final MessageSource messageSource;

    public AccountDtoResponse createAccount(Locale locale, AccountDtoRequest accountDtoRequest) {
        if (accountRepo.findByUsername(accountDtoRequest.getUsername()).isPresent()) {
            String errorMessage = messageSource.getMessage("createAccountService.existUsername", null, "This username is already used, please choose other username.", locale);
            throw new AccountException(errorMessage);
        }
        if (accountDtoRequest.getUsername().isBlank()) {
            String errorMessage = messageSource.getMessage("createAccountService.blankUsername", null, "Please fill in username!", locale);
            throw new AccountException(errorMessage);
        }
        if (accountDtoRequest.getPassword().isBlank()) {
            String errorMessage = messageSource.getMessage("createAccountService.blankPassword", null, "Please fill in password!", locale);
            throw new AccountException(errorMessage);
        }
        String encodedPassword = BCrypt.hashpw(accountDtoRequest.getPassword(), BCrypt.gensalt());
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setId(0);
        accountEntity.setUsername(accountDtoRequest.getUsername());
        accountEntity.setPassword(encodedPassword);
        accountEntity.setName(accountDtoRequest.getName());
        accountEntity.setRole(AccountRole.USER);

        accountRepo.save(accountEntity);

        return accountMapperToResponse.mapToResponse(accountEntity);
    }

}
