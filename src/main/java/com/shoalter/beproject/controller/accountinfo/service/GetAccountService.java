package com.shoalter.beproject.controller.accountinfo.service;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.repo.AccountRepo;
import com.shoalter.beproject.exception.AccountException;
import com.shoalter.beproject.mapper.accountinfo.AccountMapperToResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetAccountService {
    private final AccountRepo accountRepo;

    private final AccountMapperToResponse accountMapperToResponse;

    public AccountDtoResponse findByUsername(String username) {
        AccountEntity savedAccountInfo = accountRepo.findByUsername(username).orElseThrow(() -> {
            throw new AccountException("This username " + username + " is not exist");
        });

        return accountMapperToResponse.mapToResponse(savedAccountInfo);
    }
}
