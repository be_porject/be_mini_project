package com.shoalter.beproject.controller.accountinfo.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ResetPasswordRequest {

    @Schema(title = "登入帳號", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(title = "舊密碼", requiredMode = Schema.RequiredMode.REQUIRED)
    private String oldPassword;

    @Schema(title = "新密碼", requiredMode = Schema.RequiredMode.REQUIRED)
    private String newPassword;

    @Schema(title = "確認新密碼", requiredMode = Schema.RequiredMode.REQUIRED)
    private String confirmNewPassword;

}
