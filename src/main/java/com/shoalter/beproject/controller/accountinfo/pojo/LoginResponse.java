package com.shoalter.beproject.controller.accountinfo.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class LoginResponse {

    @Schema(title = "Token", requiredMode = Schema.RequiredMode.REQUIRED)
    private String token;
}
