package com.shoalter.beproject.controller.accountinfo.service;

import com.shoalter.beproject.config.JwtGenerator;
import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.controller.accountinfo.pojo.LoginResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.repo.AccountMapper;
import com.shoalter.beproject.exception.AccountException;
import com.shoalter.beproject.mapper.accountinfo.AccountMapperToResponse;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final AccountMapper accountMapper;

    private final JwtGenerator jwtGenerator;

    private final AccountMapperToResponse accountMapperToResponse;

    public LoginResponse loginAccount(String username, String password) {
        AccountEntity savedAccountInfo = accountMapper.findByUsername(username).orElseThrow(() -> {
            throw new AccountException("Cannot find account by this username");
        });
        if (BCrypt.checkpw(password, savedAccountInfo.getPassword())) {
            AccountDtoResponse accountDtoResponse = accountMapperToResponse.mapToResponse(savedAccountInfo);
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setToken(jwtGenerator.generateToken(accountDtoResponse));
            return loginResponse;
        } else {
            throw new AccountException("username or password is wrong");
        }
    }
}