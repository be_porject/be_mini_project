package com.shoalter.beproject.controller.accountinfo.service;

import com.shoalter.beproject.controller.accountinfo.pojo.AccountDtoResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.repo.AccountRepo;
import com.shoalter.beproject.mapper.accountinfo.AccountMapperToResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GetAllAccountService {

    private final AccountRepo accountRepo;

    private final AccountMapperToResponse accountMapperToResponse;

    public List<AccountDtoResponse> findAllUsername() {
        List<AccountEntity> savedAccountInfo = accountRepo.findAll();

        return savedAccountInfo.stream()
                .map(accountMapperToResponse::mapToResponse)
                .collect(Collectors.toList());
    }

}
