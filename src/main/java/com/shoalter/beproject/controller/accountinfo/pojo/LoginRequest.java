package com.shoalter.beproject.controller.accountinfo.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class LoginRequest {

    @Schema(title = "登入帳號", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(title = "登入密碼", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;
}
