package com.shoalter.beproject.controller;

import com.shoalter.beproject.dao.ResponseDto;
import com.shoalter.beproject.exception.AccountException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.security.auth.message.AuthException;

@Slf4j
@RestControllerAdvice
public class ExceptionController {

    private static final String COMMON_ERROR_CODE = "error";

    @ApiResponse(responseCode = "401", description = "Unauthenticated", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AuthException.class)
    public final ResponseDto<Void> handleUnauthenticated(AuthenticationException e) {
        return error("Unauthenticated: " + e.getMessage());
    }

    @ApiResponse(responseCode = "403", description = "Unauthorized", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseDto<Void> handleUnauthorizedAccess(AccessDeniedException e) {
        return error(e.getMessage());
    }

    @ApiResponse(responseCode = "500", description = "Unexpected server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({
            AccountException.class,
            Exception.class
    })
    public final ResponseDto<Void> handleUnexpectedException(Exception e) {
        log.error(e.getMessage(), e);
        return error(e.getMessage());
    }

    private ResponseDto<Void> error(String message) {
        return new ResponseDto<>(COMMON_ERROR_CODE, message);
    }
}
