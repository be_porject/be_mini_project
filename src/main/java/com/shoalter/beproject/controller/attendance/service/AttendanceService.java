package com.shoalter.beproject.controller.attendance.service;

import com.shoalter.beproject.dao.entity.AttendanceRecordEntity;
import com.shoalter.beproject.dao.entity.ClockInRecordEntity;
import com.shoalter.beproject.dao.entity.ClockOutRecordEntity;
import com.shoalter.beproject.dao.repo.AttendanceRepo;
import com.shoalter.beproject.dao.repo.ClockInRepo;
import com.shoalter.beproject.dao.repo.ClockOutRepo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AttendanceService {

    private final AttendanceRepo attendanceRepo;

    private final ClockInRepo clockInRepo;

    private final ClockOutRepo clockOutRepo;

    public void getAttendanceRecord() {
        LocalDateTime now = LocalDateTime.now();
        String oneDayBeforeNowDate = LocalDate.now().plusDays(-1).toString();

        List<ClockInRecordEntity> clockInRecordList = clockInRepo.findOldestClockInTime(oneDayBeforeNowDate);
        List<ClockOutRecordEntity> clockOutRecordList = clockOutRepo.findNewestClockOutTime(oneDayBeforeNowDate);
        List<AttendanceRecordEntity> attendanceRecordList = new ArrayList<>();

        for (ClockInRecordEntity clockInRecord : clockInRecordList) {
            for (ClockOutRecordEntity clockOutRecord : clockOutRecordList) {
                if (clockInRecord.getUsername().equals(clockOutRecord.getUsername())) {
                    AttendanceRecordEntity attendanceRecord = new AttendanceRecordEntity();
                    attendanceRecord.setUsername(clockInRecord.getUsername());
                    attendanceRecord.setClockInTime(clockInRecord.getClockInTime());
                    attendanceRecord.setClockOutTime(clockOutRecord.getClockOutTime());
                    attendanceRecord.setWorkingHours(countWorkingTime(clockInRecord.getClockInTime(), clockOutRecord.getClockOutTime()));
                    attendanceRecord.setCreateDate(now);
                    attendanceRecord.setLastModifiedDate(now);

                    attendanceRecordList.add(attendanceRecord);
                }
            }
        }
        attendanceRepo.saveAll(attendanceRecordList);
    }

    public String countWorkingTime(LocalDateTime clockIn, LocalDateTime clockOut) {
        if (clockIn != null && clockOut != null) {
            Duration duration = Duration.between(clockIn, clockOut);
            return DurationFormatUtils.formatDuration(duration.toMillis(), "HH:mm:ss");
        }
        return null;
    }

}