package com.shoalter.beproject.controller.attendance;

import com.shoalter.beproject.controller.attendance.pojo.ClockResponse;
import com.shoalter.beproject.controller.attendance.service.ClockInOrOutService;
import com.shoalter.beproject.dao.ResponseDto;
import com.shoalter.beproject.dto.TokenDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/attendance")
@Tag(name = "出勤相關")
public class AttendanceController {

    private final ClockInOrOutService clockInOrOutService;

    @Operation(summary = "出勤打卡", description = "出勤打卡")
    @PostMapping(value = "/clockInOrOut")
    public ResponseDto<ClockResponse> clockInOrOut(@AuthenticationPrincipal TokenDto token,
                                                   @RequestParam String clockType) {
        return ResponseDto.success(clockInOrOutService.clockInOrOut(token.getUsername(), clockType));
    }

}