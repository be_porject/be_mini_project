package com.shoalter.beproject.controller.attendance.service;

import com.shoalter.beproject.controller.attendance.pojo.ClockResponse;
import com.shoalter.beproject.dao.entity.AccountEntity;
import com.shoalter.beproject.dao.entity.ClockInRecordEntity;
import com.shoalter.beproject.dao.entity.ClockOutRecordEntity;
import com.shoalter.beproject.dao.repo.AccountRepo;
import com.shoalter.beproject.dao.repo.ClockInRepo;
import com.shoalter.beproject.dao.repo.ClockOutRepo;
import com.shoalter.beproject.exception.AccountException;
import com.shoalter.beproject.mapper.attendance.ClockMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClockInOrOutService {

    private final ClockInRepo clockInRepo;

    private final ClockOutRepo clockOutRepo;

    private final AccountRepo accountRepo;

    public ClockResponse clockInOrOut(String username, String clockType) {
        Optional<AccountEntity> findAccount = accountRepo.findByUsername(username);
        if (findAccount.isEmpty()) {
            throw new AccountException("This username '" + username + "' is not exist");
        }
        ClockMapper clockMapper = new ClockMapper();
        var now = LocalDateTime.now();
        switch (clockType) {
            case "clock-in":
                ClockInRecordEntity clockInEntity = new ClockInRecordEntity();
                clockInEntity.setUsername(username);
                clockInEntity.setClockInTime(now);
                clockInRepo.save(clockInEntity);

                return clockMapper.clockInMapToResponse(clockInEntity, clockType);

            case "clock-out":
                ClockOutRecordEntity clockOutEntity = new ClockOutRecordEntity();
                clockOutEntity.setUsername(username);
                clockOutEntity.setClockOutTime(now);
                clockOutRepo.save(clockOutEntity);

                return clockMapper.clockOutMapToResponse(clockOutEntity, clockType);

            default:
                throw new AccountException("The acceptable field content of type is 'clock-in' and 'clock-out'.");
        }
    }

}
