package com.shoalter.beproject.controller.attendance.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AttendanceResponse {

    @Schema(title = "使用者帳號", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(title = "上班打卡時間")
    private LocalDateTime clockInTime;

    @Schema(title = "下班打卡時間")
    private LocalDateTime clockOutTime;

    @Schema(title = "工作時間")
    private String workingHours;
}