package com.shoalter.beproject.controller.attendance.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ClockResponse {

    @Schema(title = "使用者帳號", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(title = "打卡類別", requiredMode = Schema.RequiredMode.REQUIRED)
    private String type;

    @Schema(title = "打卡時間")
    private LocalDateTime recordTime;

}
